#pragma once

#include "SceneGraph.h"
#include "tinyxml.h"
#include "Color.h"

#include <iostream>
#include <string>


class Parser
{
public:
	Parser(const SceneGraph* sceneGraph, const char* filename);

	bool initialize();

private:

	bool processGlobals();
	bool processCameras();
	bool processLights();
	bool processTextures();
	bool processAppearances();
	bool processGraph();


	bool openElement(TiXmlNode* node, TiXmlElement** element, const std::string& name);
	bool openAttribute(const TiXmlElement* element, const std::string& name, std::string& value);
	bool openAttribute(const TiXmlElement* element, const std::string& name, int& value);
	bool openAttribute(const TiXmlElement* element, const std::string& name, double& value);
	bool openAttribute(const TiXmlElement* element, const std::string& name, Color& value);
	

private:
	const SceneGraph* m_sceneGraph;

	TiXmlDocument* m_doc;
	TiXmlElement* m_anf;
	TiXmlElement* m_globals;
	TiXmlElement* m_cameras;
	TiXmlElement* m_lights;
	TiXmlElement* m_textures;
	TiXmlElement* m_appearances;
	TiXmlElement* m_graph;
};