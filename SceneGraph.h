#pragma once

#include "CGFcamera.h"
#include "CGFlight.h"
#include "GlobalArgs.h"

#include <vector>

class SceneGraph
{
public:
	SceneGraph();
	~SceneGraph();

	GlobalArgs* getGlobals() const;

protected:
	GlobalArgs* m_globals;
	CGFcamera* m_currentCamera;
	std::vector<CGFcamera*> m_cameras;
	std::vector<CGFlight*> m_lights;
	std::vector<CGFtexture*> m_textures;
	std::vector<CGFappearance*> m_appearances;
};