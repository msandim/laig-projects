#pragma once

#include "AbstractCamera.h"
#include "Axis.h"
#include "Float3.h"

class PerspectiveCamera : public AbstractCamera
{
public:
	PerspectiveCamera(const std::string& id, float near, float far, float angle, const Float3& position, const Float3& target);

private:
	float m_angle;
	Float3 m_position;
	Float3 m_target;

};