#include "OrthogonalCamera.h"

OrthogonalCamera::OrthogonalCamera(const std::string& id, float near, float far, Axis::axis_t direction, float left, float right, float top, float bottom) :
m_direction(direction),
m_left(left),
m_right(right),
m_top(top),
m_bottom(bottom),
AbstractCamera(id, near, far)
{
}