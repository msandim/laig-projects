#include "Float3.h"

Float3::Float3() :
x(0.0f), 
y(0.0f),
z(0.0f)
{
}

Float3::Float3(float x, float y, float z) :
x(x),
y(y),
z(z)
{
}