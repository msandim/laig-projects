#pragma once

struct Float3
{
	float x, y, z;

	Float3();
	Float3(float x, float y, float z);
};