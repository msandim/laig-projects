#include "Parser.h"
#include "Color.h"
#include "GlobalArgs.h"

#include <sstream>

using namespace std;

Parser::Parser(const SceneGraph* sceneGraph, const char* filename) : m_sceneGraph(sceneGraph)
{
	m_doc = new TiXmlDocument(filename);
}

bool Parser::initialize()
{
	// Load file:
	if (!m_doc->LoadFile())
	{
		cout << "Error loading file!\n";
		return false;
	}
	
	if (!openElement(m_doc, &m_anf, "anf"))
		return false;

	const unsigned int N_FUNCTIONS = 5;
	bool(Parser::* processFunctions[N_FUNCTIONS])() =
	{
		&Parser::processGlobals, &Parser::processLights, &Parser::processTextures, 
		&Parser::processAppearances, &Parser::processGraph
	};

	for (unsigned int i = 0; i < N_FUNCTIONS; i++)
		(this->*processFunctions[i])();

	return true;
}

bool Parser::processGlobals()
{
	// Get globals element:
	if (!openElement(m_anf, &m_globals, "globals"))
		return false;

#pragma region Elements
	// Prepare elements arrays:
	static const unsigned int N_ELEMENTS = 3;
	TiXmlElement* pElements[N_ELEMENTS];
	static const string elementNames[N_ELEMENTS] = { "drawing", "culling", "lighting" };

	// Get  elements:
	for (unsigned int i = 0; i < N_ELEMENTS; i++)
		if (!openElement(m_globals, &pElements[i], elementNames[i]))
			return false;
#pragma endregion
#pragma region Initialization of arrays
	static const unsigned int N_ARGS = 9;

	// Prepare array of elements
	static const unsigned int elementsIndex[N_ARGS] =
	{
		0, 0, 0,		// Drawing
		1, 1,			// Culling
		2, 2, 2, 2		// Lightning
	};

	// Prepare array of attributes:
	static const string attributes[N_ARGS] = {
		"mode", "shading", "background",				// Drawing
		"face", "order",								// Culling
		"doublesided", "local", "enabled", "ambient"	// Lightning
	};

	// Prepare array of functions:
	static void(GlobalArgs::* processFunctions[N_ARGS])(const string&) =
	{
		// Drawing:
		&GlobalArgs::setDrawingMode, &GlobalArgs::setDrawingShading,
		&GlobalArgs::setDrawingBackground,

		// Culling:
		&GlobalArgs::setCullingFace, &GlobalArgs::setCullingOrder,

		// Lightning:
		&GlobalArgs::setLightningDoublesided, &GlobalArgs::setLightningLocal,
		&GlobalArgs::setLightningEnabled, &GlobalArgs::setLightningAmbient,
	};
#pragma endregion

	// Get global args object from the scene graph:
	GlobalArgs* globals = m_sceneGraph->getGlobals();

	for (unsigned int i = 0; i < N_ARGS; i++)
	{
		// Get element (drawing, culling or lightning):
		unsigned int elementIndex = elementsIndex[i];

		// Get attribute:
		string value;
		if (!openAttribute(pElements[elementIndex], attributes[i], value))
			return false;

		// Process attribute:
		(globals->*processFunctions[i])(value);
	}

	return true;
}
bool Parser::processCameras()
{


	return true;
}
bool Parser::processLights()
{
	return true;
}
bool Parser::processTextures()
{
	return true;
}
bool Parser::processAppearances()
{
	return true;
}
bool Parser::processGraph()
{
	return true;
}


bool Parser::openElement(TiXmlNode* node, TiXmlElement** element, const string& name)
{
	*element = node->FirstChildElement(name.c_str());

	if (*element == NULL)
	{
		cout << "Element " + name + " not found." << endl;
		return false;
	}

	return true;
}

bool Parser::openAttribute(const TiXmlElement* element, const string& name, string& value)
{
	const char* attr = element->Attribute(name.c_str());
	if (attr == NULL)
	{
		cout << "Attribute " << name << " not found." << endl;
		return false;
	}

	value = string(attr);

	return true;
}

bool Parser::openAttribute(const TiXmlElement* element, const string& name, int& value)
{
	if (element->QueryIntAttribute(name.c_str(), &value) != TIXML_SUCCESS)
	{
		cout << "Attribute int parsing error - " << name << endl;
		return false;
	}

	return true;
}
bool Parser::openAttribute(const TiXmlElement* element, const string& name, double& value)
{
	if (element->QueryDoubleAttribute(name.c_str(), &value) != TIXML_SUCCESS)
	{
		cout << "Attribute double parsing error - " << name << endl;
		return false;
	}

	return true;
}
bool Parser::openAttribute(const TiXmlElement* element, const string& name, Color& value)
{
	string attr;
	if (!openAttribute(element, name, attr))
	{
		cout << "Attribute color parsing error - " << name << endl;
		return false;
	}

	// Parsing values to the color struct:
	stringstream ss(attr);
	ss >> value.x;
	ss >> value.y;
	ss >> value.z;
	ss >> value.w;

	return true;
}