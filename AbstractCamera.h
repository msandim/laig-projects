#pragma once

#include <string>

class AbstractCamera
{
public:
	AbstractCamera(const std::string& id, float near, float far);

protected:
	std::string m_id;
	float m_near;
	float m_far;
};