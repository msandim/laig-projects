#include "CGFscene.h"
#include "SceneGraph.h"

class MainScene : public CGFscene
{
public:
	MainScene(const char* filename);
	virtual ~MainScene();

	void init();
	void initCameras();
	void display();
	void update(unsigned long millis);

	SceneGraph* getSceneGraph() const;

private:
	void initializeGlobals();

private:
	SceneGraph* m_sceneGraph;
};