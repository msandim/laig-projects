#include "GlobalArgs.h"

#include <gl\gl.h>
#include <sstream>

using namespace std;

GlobalArgs::DrawingArgs GlobalArgs::getDrawingArgs() const
{
	return m_drawingArgs;
}
GlobalArgs::CullingArgs GlobalArgs::getCullingArgs() const
{
	return m_cullingArgs;
}
GlobalArgs::LightingArgs GlobalArgs::getLightningArgs() const
{
	return m_lightningArgs;
}

void GlobalArgs::setDrawingMode(const string& mode)
{
	if (mode == "fill")
		m_drawingArgs.mode = GL_FILL;
	else if (mode == "line")
		m_drawingArgs.mode = GL_LINE;
	else if (mode == "point")
		m_drawingArgs.mode = GL_POINT;
}
void GlobalArgs::setDrawingShading(const string& shading)
{
	if (shading == "flat")
		m_drawingArgs.shading = GL_FLAT;
	else if (shading == "gouraud")
		m_drawingArgs.shading = GL_SMOOTH;
}
void GlobalArgs::setDrawingBackground(const string& background)
{
	// Parsing values to the color struct:
	stringstream ss(background);
	ss >> m_drawingArgs.background.x;
	ss >> m_drawingArgs.background.y;
	ss >> m_drawingArgs.background.z;
	ss >> m_drawingArgs.background.w;
}

void GlobalArgs::setCullingFace(const std::string& face)
{
	if (face == "none")
		m_cullingArgs.face = GL_NONE;
	else if (face == "back")
		m_cullingArgs.face = GL_BACK;
	else if (face == "front")
		m_cullingArgs.face = GL_FRONT;
	else if (face == "both")
		m_cullingArgs.face = GL_FRONT_AND_BACK;
}
void GlobalArgs::setCullingOrder(const std::string& order)
{
	if (order == "ccw")
		m_cullingArgs.order = GL_CCW;
	else if (order == "cw")
		m_cullingArgs.order = GL_CW;
}

void GlobalArgs::setLightningDoublesided(const std::string& doublesided)
{
	if (doublesided == "true")
		m_lightningArgs.doublesided = true;
	else if (doublesided == "false")
		m_lightningArgs.doublesided = false;
}
void GlobalArgs::setLightningLocal(const std::string& local)
{
	if (local == "true")
		m_lightningArgs.local = true;
	else if (local == "false")
		m_lightningArgs.local = false;
}
void GlobalArgs::setLightningEnabled(const std::string& enabled)
{
	if (enabled == "true")
		m_lightningArgs.enabled = true;
	else if (enabled == "false")
		m_lightningArgs.enabled = false;
}
void GlobalArgs::setLightningAmbient(const std::string& ambient)
{
	// Parsing values to the color struct:
	stringstream ss(ambient);
	ss >> m_lightningArgs.ambient.x;
	ss >> m_lightningArgs.ambient.y;
	ss >> m_lightningArgs.ambient.z;
	ss >> m_lightningArgs.ambient.w;
}