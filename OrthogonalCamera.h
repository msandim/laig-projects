#pragma once

#include "AbstractCamera.h"
#include "Axis.h"

class OrthogonalCamera : public AbstractCamera
{
public:
	OrthogonalCamera(const std::string& id, float near, float far, Axis::axis_t direction, float left, float right, float top, float bottom);

private:
	Axis::axis_t m_direction;
	float m_left;
	float m_right;
	float m_top;
	float m_bottom;
};