#include "Sphere.h"

#include <gl\gl.h>

using namespace std;

Sphere::Sphere()
{
	unsigned int segments = 32;
	unsigned int rings = 16;

	// Pre-calculate everything:
	calculateVertices(segments, rings);
	calculateIndices(segments, rings);
	calculateNormals();
	calculateTextels();

	m_material = CGFappearance();
	m_material.setTexture("landscape.jpg");

	m_position = Float3(0.0f, 0.0f, 0.0f);
	m_rotation = Float3(0.0f, 0.0f, 0.0f);
	m_scale = Float3(0.0f, 0.0f, 0.0f);
}
Sphere::~Sphere()
{
}

void Sphere::draw()
{
	glPushMatrix();

	// Apply transforms:
	glTranslatef(m_position.x, m_position.y, m_position.z);
	glRotatef(m_rotation.x, 1.0f, 0.0f, 0.0f);
	glRotatef(m_rotation.y, 0.0f, 1.0f, 0.0f);
	glRotatef(m_rotation.z, 0.0f, 0.0f, 1.0f);
	glScalef(m_scale.x, m_scale.y, m_scale.z);
	
	// Apply material:
	m_material.apply();

	// Enable vertex, normal and textels arrays:
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	// Set pointers to vertex, normal and textels arrays:
	glVertexPointer(3, GL_FLOAT, 0, &m_vertices[0]);
	glNormalPointer(GL_FLOAT, 0, &m_normals[0]);
	glTexCoordPointer(2, GL_FLOAT, 0, &m_textels[0]);

	// Draw all:
	glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_SHORT, &m_indices[0]);

	// Disable vertex, normal and textels array:
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glPopMatrix();
}

void Sphere::calculateVertices(unsigned int segments, unsigned int rings)
{
	// Theta -> [0, 90] <-
	// Phi -> [0, 360[ ->

	float deltaPhi = 2 * (float) acos(-1) / segments;
	float deltaTheta = (float) acos(-1) / (2 * rings);

	m_vertices = std::vector<GLfloat>((segments * rings + 1) * 3);
	m_vertices[0] = 0.0f;
	m_vertices[1] = 0.0f;
	m_vertices[2] = 1.0f;

	unsigned int index = 3;
	for (unsigned int s = 0; s < segments; s++)
	{
		for (unsigned int r = 1; r <= rings; r++)
		{
			float theta = deltaTheta * r;
			float phi = deltaPhi * s;

			m_vertices[index] = sin(theta) * cos(phi);
			m_vertices[index + 1] = sin(theta) * sin(phi);
			m_vertices[index + 2] = cos(theta);

			index += 3;
		}
	}
}

void Sphere::calculateIndices(unsigned int segments, unsigned int rings)
{
	unsigned int nTriangles = segments * (1 + 2 * (rings - 1));

	m_indices = std::vector<GLushort>(3 * nTriangles);
	for (unsigned int i = 0; i < segments; i++)
	{
		unsigned int index = i * 3;

		m_indices[index] = -1;
		m_indices[index + 1] = ((i + 1) * rings) % (rings * segments);
		m_indices[index + 2] = i * rings;
	}

	for (unsigned int i = 0; i < segments; i++)
	{
		unsigned int index = 3 * segments + i * 6;

		m_indices[index] = i * rings;
		m_indices[index + 1] = ((i + 1) * rings) % (rings * segments);
		m_indices[index + 2] = i * rings + 1;

		m_indices[index + 3] = i * rings + 1;
		m_indices[index + 4] = ((i + 1) * rings) % (rings * segments);
		m_indices[index + 5] = ((i + 1) * rings + 1) % (rings * segments);
	}

	for (unsigned int i = 0; i < 2 * segments * (rings - 2) * 3; i++)
		m_indices[9 * segments + i] = m_indices[3 * segments + i] + 1;

	for (unsigned int i = 0; i < m_indices.size(); i++)
		m_indices[i]++;
}

void Sphere::calculateNormals()
{
	m_normals = std::vector<GLfloat>(m_vertices.size());

	for (unsigned int i = 0; i < m_normals.size(); i++)
		m_normals[i] = -m_vertices[i];
}

void Sphere::calculateTextels()
{
	m_textels = std::vector<GLfloat>(2 * m_vertices.size() / 3);

	for (unsigned int i = 0; i < m_textels.size() / 2; i++)
	{
		unsigned int verticesI = i * 3;
		unsigned int textelsI = i * 2;

		m_textels[textelsI] = (m_vertices[verticesI] + 1) / 2.0f;
		m_textels[textelsI + 1] = (m_vertices[verticesI + 1] + 1) / 2.0f;
	}
}