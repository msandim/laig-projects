#include "PerspectiveCamera.h"

PerspectiveCamera::PerspectiveCamera(const std::string& id, float near, float far, float angle, const Float3& position, const Float3& target) :
m_angle(angle),
m_position(position),
m_target(target),
AbstractCamera(id, near, far)
{
}