#include "CGFapplication.h"
#include "MainScene.h"
#include "CGFinterface.h"

#include <iostream>

using namespace std;

int main(int argc, char* argv [])
{
	if (argc != 2)
		return -1;

	char* filename = argv[1];
	MainScene* mainScene = new MainScene(filename);

	CGFapplication app = CGFapplication();
	try
	{
		// Initialize app:
		app.init(&argc, argv);

		// Set the scene:
		app.setScene(mainScene);
		app.setInterface(new CGFinterface());

		// Run the app:
		app.run();
	}
	catch (GLexception& ex)
	{
		cout << "Error: " << ex.what();
		return -1;
	}
	catch (exception& ex)
	{
		cout << "Unexpected error: " << ex.what();
		return -1;
	}

	return 0;
}