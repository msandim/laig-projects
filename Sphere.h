#pragma once

#include "CGFappearance.h"
#include "Float3.h"

#include <vector>

class Sphere
{
public:
	Sphere();
	~Sphere();

	void draw();

private:
	void calculateVertices(unsigned int segments, unsigned int rings);
	void calculateIndices(unsigned int segments, unsigned int rings);
	void calculateNormals();
	void calculateTextels();

private:
	std::vector<float> m_vertices;
	std::vector<unsigned short> m_indices;
	std::vector<float> m_normals;
	std::vector<float> m_textels;

	Float3 m_position;
	Float3 m_rotation;
	Float3 m_scale;

	CGFappearance m_material;
};