#pragma once

#include "Color.h"

#include <string>

class GlobalArgs
{
public:
	// Drawing Args:
	struct DrawingArgs
	{
		unsigned int mode;
		unsigned int shading;
		Color background;
	};

	// Culling Args:
	struct CullingArgs
	{
		unsigned int face;
		unsigned int order;
	};

	// Lightning Args
	struct LightingArgs
	{
		bool doublesided;
		bool local;
		bool enabled;
		Color ambient;
	};

public:
	DrawingArgs getDrawingArgs() const;
	CullingArgs getCullingArgs() const;
	LightingArgs getLightningArgs() const;

	void setDrawingMode(const std::string& mode);
	void setDrawingShading(const std::string& shading);
	void setDrawingBackground(const std::string& background);

	void setCullingFace(const std::string& face);
	void setCullingOrder(const std::string& order);

	void setLightningDoublesided(const std::string& doublesided);
	void setLightningLocal(const std::string& local);
	void setLightningEnabled(const std::string& enabled);
	void setLightningAmbient(const std::string& ambient);

private:
	DrawingArgs m_drawingArgs;
	CullingArgs m_cullingArgs;
	LightingArgs m_lightningArgs;
};