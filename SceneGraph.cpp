#include "SceneGraph.h"

SceneGraph::SceneGraph()
{
	m_globals = new GlobalArgs();
}
SceneGraph::~SceneGraph()
{
	if (m_globals)
	{
		delete m_globals;
		m_globals = 0;
	}
}

GlobalArgs* SceneGraph::getGlobals() const
{
	return m_globals;
}