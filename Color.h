#pragma once

struct Color
{
	double x;
	double y;
	double z;
	double w;

	Color();
	Color(double x, double y, double z, double w);

	bool operator==(const Color& other);

private:
	static const float s_DIFF;
};