#include "MainScene.h"
#include "Parser.h"

MainScene::MainScene(const char* filename)
{
	m_sceneGraph = new SceneGraph();

	Parser parser(m_sceneGraph, filename);
	if (!parser.initialize())
		throw exception("Error while initializing parser!");
}
MainScene::~MainScene()
{
	if (m_sceneGraph)
	{
		delete m_sceneGraph;
		m_sceneGraph = 0;
	}
	cout << "Main scene destructor";
}

void MainScene::init()
{
	initializeGlobals();
}
void MainScene::initCameras()
{
}
void MainScene::display()
{
}
void MainScene::update(unsigned long millis)
{
}

SceneGraph* MainScene::getSceneGraph() const
{
	return m_sceneGraph;
}

void MainScene::initializeGlobals()
{
	// Get global args:
	GlobalArgs* globalArgs = m_sceneGraph->getGlobals();

#pragma region Drawing Args

	// Get drawing attribute:
	GlobalArgs::DrawingArgs drawingArgs = globalArgs->getDrawingArgs();

	// Set drawing mode (point, line, fill):
	glPolygonMode(GL_FRONT_AND_BACK, drawingArgs.mode);

	// Set shading (flat or smooth):
	glShadeModel(drawingArgs.shading);

	// Set background color:
	Color* background = &drawingArgs.background;
	glClearColor(background->x, background->y, background->z, background->w);

#pragma endregion

#pragma region Culling Args

	// Get culling attribute:
	GlobalArgs::CullingArgs cullingArgs = globalArgs->getCullingArgs();

	// Set culling face (none, back, front or both):
	glCullFace(cullingArgs.face);

	// Set culling order (counterclockwise / clockwise):
	glFrontFace(cullingArgs.order);

#pragma endregion

#pragma region Lighting Args

	// Get lighting args:
	GlobalArgs::LightingArgs lightingArgs = globalArgs->getLightningArgs();

	// Set doubledsided:
	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, lightingArgs.doublesided);

	// Set local: TODO
	

	// Set enabled:
	if (lightingArgs.enabled)
		glEnable(GL_LIGHTING);
	else
		glDisable(GL_LIGHTING);

	// Set ambient:
	Color* ambient = &lightingArgs.ambient;
	GLfloat ambientParam[4] = { ambient->x, ambient->y, ambient->z, ambient->w };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientParam);

#pragma endregion
}