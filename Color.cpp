#include "Color.h"

const float Color::s_DIFF = 0.001f;

Color::Color() : x(0.0), y(0.0), z(0.0), w(0.0)
{
}

Color::Color(double x, double y, double z, double w) : x(x), y(y), z(z), w(w)
{
}

bool Color::operator==(const Color& other)
{
	return (other.x - x) < s_DIFF
		&& (other.y - y) < s_DIFF
		&& (other.z - z) < s_DIFF
		&& (other.w - w) < s_DIFF;
}